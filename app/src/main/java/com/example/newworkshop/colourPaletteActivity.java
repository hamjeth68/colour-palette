package com.example.newworkshop;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class colourPaletteActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView red, green, blue, orange, black, holo, output;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colour_pallate2);

        red = findViewById(R.id.red);
        red.setOnClickListener(this);
        green = findViewById(R.id.green);
        green.setOnClickListener(this);
        blue = findViewById(R.id.blue);
        blue.setOnClickListener(this);
        orange = findViewById(R.id.orange);
        orange.setOnClickListener(this);
        black = findViewById(R.id.black);
        black.setOnClickListener(this);
        holo = findViewById(R.id.holo);
        holo.setOnClickListener(this);

        output = findViewById(R.id.output);
    }
    @Override
     public void onClick(View V){
           switch (V.getId()){
               case R.id.red:
                   output.setBackgroundColor(Color.parseColor( "#ED3211"));
                   break;
               case R.id.green:
                   output.setBackgroundColor(Color.parseColor( "#23F706"));
                   break;
               case R.id.purple:
                   output.setBackgroundColor(Color.parseColor( "#7008CD"));
                   break;
               case R.id.blue:
                   output.setBackgroundColor(Color.parseColor( "#066CF7"));
                   break;
               case R.id.yellow:
                   output.setBackgroundColor(Color.parseColor( "#F3F706"));
                   break;
               case R.id.black:
                   output.setBackgroundColor(Color.parseColor( "#000000"));
                   break;
               case R.id.orange:
                   output.setBackgroundColor(Color.parseColor( "#F16104"));
                   break;
               case R.id.holo:
                   output.setBackgroundColor(Color.parseColor( "#EC6B51"));
                   break;


           }
    }

}

